package ru.gva.box;

/**
 * @author Gavrikov V.A. Group 15IOT18
 */
public class BoxColor extends Box {
    public int red;
    public int green;
    public int blue;

    public BoxColor(int width, int along, int height, int red, int green, int blue) {
        super(width, along, height);
        this.red = red;
        this.green = green;
        this.blue = blue;
    }


    public BoxColor() {
        this(1, 1, 1, 1, 1, 1);

    }


    @Override
    public String toString() {
        return "BoxColor{" +
                "width=" + width +
                ", along=" + along +
                ", height=" + height +
                "red=" + red +
                ", green=" + green +
                ", blue=" + blue +
                '}';
    }
}
