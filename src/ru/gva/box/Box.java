package ru.gva.box;

/**
 * @author Gavrikov V.A. Group 15IOT18
 */
public class Box {
    public int width;
    public int along;
    public int height;

    public Box(int width, int along, int height) {
        this.width = width;
        this.along = along;
        this.height = height;
    }


    public Box() {
        this(1, 1, 1);

    }

    public double volume() {
        return (double) this.height * this.along * this.width;
    }


}