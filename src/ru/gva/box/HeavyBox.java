package ru.gva.box;



/**
 * @author Gavrikov V.A. Group 15IOT18
 */
public class HeavyBox extends BoxColor {

    public int weight;

    public HeavyBox(int width, int along, int height, int red, int green, int blue, int weight) {
        super(width, along, height, red, green, blue);
        this.weight = weight;
    }

    public HeavyBox() {
        this(1, 1, 1, 1, 1, 1, 1);
    }

    @Override
    public String toString() {
        return "HeavyBox{" +
                "width=" + width +
                ", long=" + along +
                ", height=" + height +
                ", red=" + red +
                ", green=" + green +
                ", blue=" + blue +
                ", weight=" + weight +
                '}';
    }

    /**
     * Медо находит плотность обьекта
     *
     * @return массу обьекта разделеную на его обьем.
     */
    public double density(){
        return (double) this.weight/this.volume();
    }

    /**
     * Метод для генерирование данных.
     */
    public void toGetTheValue() {
        this.weight = 1 + (int) (Math.random() * (20));
        this.along = 1 + (int) (Math.random() * (20));
        this.height =1 + (int) (Math.random() * (20));
        this.width = 1 + (int) (Math.random() * (20));
        this.red =(int) (Math.random() * (256));
        this.green =(int) (Math.random() * (256));
        this.blue =(int) (Math.random() * (256));

    }


}
