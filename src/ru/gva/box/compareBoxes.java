package ru.gva.box;

/**
 * Данный класс служит для  нахождения самой высокой плотностью.
 * 
 * @author Gavrikov V.A. Group 15IOT18
 */
public class compareBoxes {
    

    public static void main(String[] args) {
        HeavyBox[] boxes = new HeavyBox[10];
        input(boxes);
        System.out.println("Коробка с максимальной плотностью ");
        System.out.println(maxDensity(boxes));
    }

    /**
     * Метод для заполнения массива обьектов.
     *
     * @param boxes массив обьектов
     */
    private static void input(HeavyBox[] boxes) {
        for (int i = 0; i <boxes.length ; i++) {
            boxes[i] = new HeavyBox();
            boxes[i].toGetTheValue();
            System.out.println(boxes[i]);
        }
    }

    /**
     * Данный метод ищет в массиве обьект с самой высокой плотностью.
     *
     * @param boxes массив обьектов
     * @return обьект с самой высокой плотностью.
     */
    private static HeavyBox maxDensity(HeavyBox[] boxes) {
        double max = boxes[0].density();
        int index = 0;
        for (int i = 1; i <boxes.length ; i++) {
            if (max<boxes[i].density()){
                max = boxes[i].density();
                index = i;
            }

        }
        return boxes[index];
    }
}
